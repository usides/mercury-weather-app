# Mercury Academy Weather App

[DEMO](https://usides.github.io/mercury-weather-app/)

Добрый день, Mercury Development Team!

Большое спасибо за отлично подготовленное и оформленное тестовое задание.

До начала работы над вашим заданием я не имел опыта работы с фреймворками / библиотеками для разработки пользовательских интерфейсов и языком TypeScript. Мое обучение front-end технологиям складывалось так, что именно в момент получения задания я стоял на пороге выбора своего первого фреймворка. Очевидно, что ваше задание помогло сделать выбор в пользу React =). Ввиду того, что я не мог оценить сколько времени мне понадобится, чтобы изучить и применить базовые знания React и TypeScript, в написании этого приложения я использовал MVP подход.

Моей целью было написать минимально функционирующее приложение на React с последующим его улучшением согласно техническим требованиям и дизайну.

Несколько дней я потратил на изучение документации React и Create React App, прежде чем смог сделать обособленный компонент с логикой и подключёнными стилями.

Подключение к API не вызвало затруднений и когда я немного освоился с работой компонентов я написал приложение используя только на нативные элементы контроля формы.
В таком виде приложение не соответствовало дизайну но обладало keyboard a11y и было без явных семантических проблем.

Следующим шагом было внедрение TypeScript.
На этом этапе у меня было очень много проблем. Я немного ознакомился с документацией и основными принципами, но такого поверхностного ознакомления явно не хватило, чтобы осознанно переписать приложение. Вся моя работа в итоге сводилась к тому, чтобы исправить ошибки компилятора любой ценой. В ходе такой работы код стал содержать некоторое количество нелогичных и излишних строк.
Я обязательно вернусь к изучению TS с планомерным и последовательным подходом.

Последним этапом я внедрил компонент выбора города, который наиболее соответствует дизайн-макету и реализовал на нем базовое управление с клавиатуры. Применение этого компонента явно не пошло семантике на пользу.

## Список выполненных требований:

- [x] Проект должен быть написан на React + TypeScript.
- [x] Для компонентов нужно использовать React hooks.
- [x] Должны быть использованы только самостоятельно разработанные компоненты
      и нативные HTML-контролы. Использование UI-фреймворков или других готовых
      решений не допускается.
- Верстка должны быть:
- [x] адаптивной,
- [ ] семантически правильной. (Вызывает сомнения из-за кастомного компонента выбора даты)
- [x] соответствовать методологии БЭМ.
- [x] Должна присутствовать реальная интеграция с внешним API.
- [x] Необходимо использовать ESLint со старнадрным конфигом. (Использован ts-standard)
- [x] Исходный код выполненного задания вместе со всей историей коммитов должен
      быть размещен на GitHub.
- [x] Приложение должно быть доступно на pages.github.com.
- [x] Проект должен содержать корневой README.md файл с пояснениями
